

class Car:
    def __init__(self, manufacturer="Ford", color="Blue", year="2012"):
        self.manufacturer = manufacturer
        self.color = color
        self.year = year
        # implicitly, the car is not started.
        self.ignition = False
        self.moving = False

    # turn on the ignition
    def ignition_on(self):
        self.ignition = True
        print("Car performed ignition on!")

    # turn off the ignition
    def ignition_off(self):
        self.ignition = False
        print("Car performed ignition off!")

    # the car starts moving
    def start_moving(self):
        self.moving = True
        print("Car started moving!")

    # the car stops moving
    def stop_moving(self):
        self.moving = False
        print("Car stopped moving!")