# TESTS FILE
# DO NOT MODIFY THIS FILE!
from src import car

def test_car_implementation():
    c = car.Car()

    # check that initially, the ignition is OFF and the car is stationary
    assert c.ignition == False
    assert c.moving == False

    # check that you cannot move the car with ignition OFF
    c.start_moving()
    assert c.ignition == False
    assert c.moving == False

    # perform ignition ON and the car remains stationary
    c.ignition_on()
    assert c.ignition == True
    assert c.moving == False

    # start moving and check that the car is in motion
    c.start_moving()
    assert c.ignition == True
    assert c.moving == True

    # try to perform ignition OFF while car is in motion
    # this should never happen!
    c.ignition_off()
    assert c.ignition == False
    assert c.moving == True

    # halt the car, with the ingition ON
    c.stop_moving()
    assert c.ignition == True
    assert c.moving == False

    # car is stationary, now perform ignition OFF
    c.ignition_off()
    assert c.ignition == False
    assert c.moving == False

    print("====Tests passed successfully!====")
    print("==Car implementation is robust!===")
    print("========Congratulations!==========")